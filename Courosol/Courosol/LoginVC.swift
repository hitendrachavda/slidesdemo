//
//  LoginVC.swift
//  Covid19App
//
//  Created by Hitendra on 02/07/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var headerView: UIView?
    @IBOutlet weak var txtEmailorMobile: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerView?.applyGradient(colors: [UIColor(named: "colorPrimaryDark")!.cgColor, UIColor.init(red: 125.0/255.0, green: 146.0/255.0, blue: 184.0/255.0, alpha: 1.0).cgColor], locations: [0.0,1.0], direction: .leftToRight)
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        self.headerView?.updateGradientFrame()
        self.headerView?.roundCorners([.bottomLeft, .bottomRight], radius: (self.headerView?.frame.size.height)!/2)
    }
    
    @IBAction func submit(_ sender : UIButton){
        self.view.endEditing(true)
        var message : String = ""
        if txtEmailorMobile?.text!.trim().count == 0 {
            message = "Enter Mobile Number"
        }
        else if txtPassword?.text!.trim().count == 0 {
            message = "Enter password"
        }
        
        if (message.count > 0){
            AlertController.shared.showAlert(fromVC: self, title: "Covid19", message: message, buttonTitles: ["Ok"],handlars:nil)
            return
        }
        let loginparam : String = {
            let mobile = "mobile="+"\(txtEmailorMobile!.text!)"
            let password = "password="+"\(txtPassword!.text!)"
            return "\(mobile)&\(password)"
        }()

        ActivityView.sharedInstance.showLoading(vc: self)
        WebServiceManager.sharedInstance.postServiceCall(name: "login", param: loginparam) {  (data, error, responeError) in
            ActivityView.sharedInstance.hideLoading(vc: self)
            guard error == nil, let responseData = data  else {
                AlertController.shared.showAlert(fromVC: self, title: "Covid19", message: error!.localizedDescription, buttonTitles: ["Ok"],handlars:nil)
                return
            }
            if(responeError!.count > 0){
                AlertController.shared.showAlert(fromVC: self, title: "Covid19", message: responeError!, buttonTitles: ["Ok"],handlars:nil)
                return
            }
            self.storeLoginUserInfo(responseData)
            UserDefaults.standard.setValue(responseData["token"], forKey: "UserToken")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC_sid") as! HomeVC
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }
    
    func storeLoginUserInfo(_ responseData : [String : Any]){
        let visitorDict = responseData["visitor"] as! NSDictionary
        var userInfo = [ String : Any?]()
        let user_id = visitorDict["user_id"] as? NSNumber
        userInfo["user_id"] = user_id
        let visitor_id = visitorDict["id"] as? NSNumber
        userInfo["visitor_id"] = visitor_id
        UserDefaults.standard.setValue(userInfo, forKey: "UserInfo")
    }
    
    @IBAction func signUp(_ sender : UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let regVC = storyboard.instantiateViewController(withIdentifier: "RegistrationVC_sid") as! RegistrationVC
        self.navigationController?.pushViewController(regVC, animated: true)
    }
    @IBAction func termsCondition(_ sender : UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let companPage = storyboard.instantiateViewController(withIdentifier: "CompanyPageVC_sid") as! CompanyPageVC
        self.navigationController?.pushViewController(companPage, animated: true)
    }
}

extension LoginVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
