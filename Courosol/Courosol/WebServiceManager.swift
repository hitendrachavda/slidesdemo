//
//  WebServiceManager.swift
//  MVVM
//
//  Created by Hitendra on 04/06/20.
//

import UIKit
import Foundation

private var AppUrl = "https://reqbin.com/sample/post/json"

class WebServiceManager: NSObject {

    static let sharedInstance = WebServiceManager()
    
    func setSericeAppUrl(appUrl : String){
        AppUrl = appUrl + "/api/"
        print(AppUrl)
    }
    
    func getAPIServiceCall(urlString : String, complition : @escaping(Data?, Error?) -> ()){
        
        let url = URL(string: urlString)
        guard let requestUrl = url else {
            fatalError()
        }
        let request = URLRequest(url: requestUrl)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if error == nil{
                    if let responseData = data{
                        complition(responseData,nil)
                    }
                }
                else{
                    complition(nil,error)
                }
            }
        }
        task.resume()
    }

    func postServiceCall(name : String, param : String , complition : @escaping([String : Any]?, String?) -> ()){
        
        let fullPath = "\(AppUrl)"+"\(name)"
        let url = URL(string: fullPath)
        guard let requestUrl = url else {
            fatalError()
        }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
                
        let token = UserDefaults.standard.value(forKey: "UserToken") as? String
        if token != nil && token!.count > 0{
            let paramToken = "Bearer \(token ?? "")"
            request.addValue(paramToken, forHTTPHeaderField: "Authorization")
        }
        if(param.count > 0){
            request.httpBody = param.data(using: String.Encoding.utf8)
        }
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if error == nil{
                    if let responseData = data{
                        let json = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String: Any]
                        if json != nil {
                            complition(json,"")
                        }
                        else{
                            complition(nil,"something went wrong")
                        }
                    }
                }
                else{
                    complition(nil,error?.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    func isResponseDataError(_ responseData :  Data)-> (resdata : [String : Any]?, erroMsg : String?) {
        let json = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String: Any]
        guard json != nil else {
            return(["Data" : "Something went wrong"],"Something went wrong")
        }
        return (json,"")
    }

    func getServiceCall(name : String, complition : @escaping([String : Any]?, String?) -> ()){
        
        let fullPath = "\(AppUrl)"+"\(name)"
        let url = URL(string: fullPath)
        guard let requestUrl = url else {
            fatalError()
        }
        var request = URLRequest(url: requestUrl)
        let token = UserDefaults.standard.value(forKey: "UserToken") as? String
        if token != nil && token!.count > 0{
           let paramToken = "Bearer \(token ?? "")"
            request.addValue(paramToken, forHTTPHeaderField: "Authorization")
        }

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                if error == nil{
                    if let responseData = data{
                        let json = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String: Any]
                        if json != nil {
                            complition(json,"")
                        }
                        else{
                            complition(nil,"something went wrong")
                        }
                    }
                }
                else{
                    complition(nil,error?.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
   
    func downlodImage(_ imgUrl : String , complition : @escaping(NSData?,Error?) -> ()){
        var responseData : Data? = nil
        URLSession.shared.dataTask(with: URL(string: imgUrl)!) { (data, response, error) in
            if error == nil{
                DispatchQueue.main.async {
                    responseData = data
                    complition(responseData as NSData?,nil)
                }
            }
        }.resume()
    }
    
}
let imageCache = NSCache<NSString, UIImage>()

class CustomImageView: UIImageView {
    var imageUrlString: String?
    
    func downloadImageFrom(withUrl urlString : String) {
        imageUrlString = urlString
        
        let url = URL(string: urlString)
        self.image = nil
        
        if let cachedImage = imageCache.object(forKey: urlString as NSString) {
            self.image = cachedImage
            return
        }
        else{
            self.image = nil
        }
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error == nil {
                DispatchQueue.main.async {
                    if let image = UIImage(data: data!) {
                        imageCache.setObject(image, forKey: NSString(string: urlString))
                        if self.imageUrlString == urlString {
                            self.image = image
                        }
                    }
                }
            }
        }).resume()
    }
}
