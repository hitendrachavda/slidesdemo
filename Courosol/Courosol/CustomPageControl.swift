//
//  CustomPageControl.swift
//  Courosol
//
//  Created by Hitendra on 30/10/21.
//

import UIKit

class PageIndicator : UICollectionViewCell{
    
    fileprivate var bgView1 : UIView = UIView(frame:CGRect.zero)
    fileprivate var bgView2 : UIView = UIView(frame:CGRect.zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup(frame: frame)
    }
    func hideselected(){
        self.bgView2.isHidden = true
    }
    func showselected(){
        self.bgView1.isHidden = false
    }
    func setup(frame: CGRect) {
        
        self.addSubview(bgView1)
        self.addSubview(bgView2)
        self.bgView1.translatesAutoresizingMaskIntoConstraints = false
        
        self.bgView1.heightAnchor.constraint(equalToConstant: frame.height).isActive=true
        self.bgView1.widthAnchor.constraint(equalToConstant: frame.height).isActive = true
        self.bgView1.backgroundColor = .white
        
        self.bgView2.translatesAutoresizingMaskIntoConstraints = false
        self.bgView2.leadingAnchor.constraint(equalTo: self.bgView1.leadingAnchor, constant: 1.5).isActive = true
        self.bgView1.layer.masksToBounds = true
        self.bgView1.layer.cornerRadius = frame.height/2
        
        self.bgView2.trailingAnchor.constraint(equalTo: self.bgView1.trailingAnchor, constant: -1.5).isActive = true
        self.bgView2.topAnchor.constraint(equalTo: self.bgView1.topAnchor, constant: 1.5).isActive = true
        self.bgView2.bottomAnchor.constraint(equalTo: self.bgView1.bottomAnchor, constant: -1.5).isActive = true
        self.bgView2.backgroundColor = .green
        self.bgView2.layer.masksToBounds = true
        self.bgView2.layer.cornerRadius = frame.height/2
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol CustomPageControlDelegate : AnyObject  {
    func selectedpage(_ index : Int)
}

class CustomPageControl: UIView {
    
    private var collectionView : UICollectionView?
    weak var delegate : CustomPageControlDelegate?
    
    var items:[slides] = []
    
    var selectedPage = 1
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundColor = .clear
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configurePager()
    }

    func setSelectedIndex(_ index : Int){
        selectedPage = index
        self.collectionView?.reloadData()
    }
    func configurePager(){
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .horizontal
        collectionView!.setCollectionViewLayout(layout, animated: true)
        collectionView!.delegate = self
        layout.minimumLineSpacing = 5.0
        layout.minimumInteritemSpacing = 0.0
        collectionView!.dataSource = self
        collectionView!.backgroundColor = UIColor.clear
        self.collectionView!.register(PageIndicator.self, forCellWithReuseIdentifier: "PageIndicator")
        self.collectionView!.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(collectionView!)
        self.collectionView!.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.collectionView!.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        self.collectionView!.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.collectionView!.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
   }
}

extension CustomPageControl : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PageIndicator", for: indexPath) as! PageIndicator
        cell.bgView1.isHidden = false
        cell.bgView2.isHidden = true
        if(selectedPage == indexPath.row + 1){
            cell.bgView2.isHidden = false
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: collectionView.frame.height, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedPage = indexPath.row + 1
        self.collectionView?.reloadData()
        delegate?.selectedpage(indexPath.row)
        
    }
}
