//
//  LandingViewController.swift
//  Citi Live Well 2019 SV
//
//  Created by Apple Developer on 9/26/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import GoogleTagManager
import Firebase
import MRProgress
import WebKit


class HorizontalCell : UICollectionViewCell, WKUIDelegate{
    
    @IBOutlet weak var viewTitleHeight : NSLayoutConstraint!
    @IBOutlet weak var tiltetop : NSLayoutConstraint!
    @IBOutlet weak var viewDesc : UIView!
    @IBOutlet weak var lbltilte : UILabel!
    @IBOutlet weak var lblDetail : UILabel!
    @IBOutlet weak var imgview : AsyncImageView?
    
    lazy var webView: WKWebView = {
        let webConfiguration = WKWebViewConfiguration()
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.bringSubviewToFront(webView)
    }

    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        let configuration = WKWebViewConfiguration()
        configuration.allowsInlineMediaPlayback = true
        webView = WKWebView(frame: frame,configuration: configuration)
        self.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        webView.heightAnchor.constraint(equalToConstant: self.frame.height/2).isActive = true
       
    }
    
    func hideVideo(){
        webView.isHidden = true
    }
    func loadVideo(_ urlString : String){
        let url = URL(string: urlString)!
        webView.load(URLRequest(url: url))
        webView.isHidden = false
    }
}


class LandingViewController: BaseViewController, UIToolbarDelegate{

    @IBOutlet weak var lblusername: UILabel!
    @IBOutlet weak var lbllocation: UILabel!
    @IBOutlet weak var btnUserInfo: UIButton!
    @IBOutlet weak var courosolCollection : UICollectionView!
    @IBOutlet weak var customPageControl : CustomPageControl!
    
    @IBOutlet weak var viewChallangeButtons : UIView!
    @IBOutlet weak var currentChallangeHistory : UIStackView!
    @IBOutlet weak var challangeRegisterView : UIView!
    
    @IBOutlet weak var coworkerChallages : UIStackView!
    @IBOutlet weak var coworkerChallangesRegister : UIView!
    
    var items : [LWSlides] = []
    
    weak var delegate: NavigationDelegate?
    
    weak var myProgress : MyProgressViewController?

    lazy var rightButtonsContinerViewController = LWUtil.getViewController("RightButtonsContainer") as! RightButtonsContainerViewController

    var featuredChallenge: LWChallengeCMS!
    private let cpageData = LWMain.data.challengesPage!
    private let lpageData = LWMain.data.landingPage!
    

    var user: LWUser = LWMain.data.user! {
        didSet {
            LWMain.data.user! = user
        }
    }
    var selectedOffice: LWOffice?
    
    override func viewWillAppear(_ animated: Bool) {
        Analytics.setScreenName("Home", screenClass: String(describing: type(of: self)))
        if self.selectedOffice == nil && LWMain.data.offices == nil {
            self.loadOffices()
        }
        loadUserFromAPI();
    }
    
    func checkChallnageRegistration(){
        self.featuredChallenge = cpageData.featuredChallenge
        guard featuredChallenge != nil else { return }
        user.coworkerChallengeOptIn = false
        if(self.featuredChallenge.isRegistered || user.coworkerChallengeOptIn == true){
            print(self.featuredChallenge.isRegistered,user.coworkerChallengeOptIn)
            self.viewChallangeButtons.isHidden = false
            self.currentChallangeHistory.isHidden = false
            self.challangeRegisterView.isHidden = true
        }
        else{
            self.viewChallangeButtons.isHidden = true
            self.currentChallangeHistory.isHidden = true
            self.challangeRegisterView.isHidden = false
        }
    }
    private func loadOffices(){
        //NOTE: only do this if LWCMSAPI hasn't loaded the offices and selectedOffices is nil after trying to get it from LWUtils
        MRProgressOverlayView.showOverlayAdded(to: self.view , title: "Loading...", mode: .indeterminateSmallDefault, animated: false)
        weak var weakSelf = self
        LWCMSAPI.offices.promise().done { offices in
            print(offices[0])
            LWMain.data.offices = offices
            MRProgressOverlayView.dismissOverlay(for: self.view ,animated: true)
            weakSelf?.selectedOffice = LWUtil.getOffice(id:  LWMain.data.user?.officeLocationId)
            weakSelf?.setUIValues()
            
        }.catch { error in
            MRProgressOverlayView.dismissOverlay(for: self.view,animated: true)
        }.finally {
            MRProgressOverlayView.dismissOverlay(for: self.view,animated: true)
        }
    }
    
    @IBAction func registerFitnessChallange(_ sender : UIButton){
        print(featuredChallenge.id! as String)
        self.registerFitnessChallenge(challenge: self.featuredChallenge)
    }
    func registerFitnessChallenge(challenge: LWChallengeCMS) {
        MRProgressOverlayView.showOverlayAdded(to: UIApplication.shared.keyWindow, title: "Loading Challenge...", mode: .indeterminateSmallDefault, animated: false)
        challenge.promiseForFitnessChallengeRegistration(id: challenge.id!).done { fitnessChallenge in
            LWFitnessChallenge.current = fitnessChallenge
            
            // Push Fitness Registration View Controller
            let storyboard = UIStoryboard(name: "FitnessRegistration", bundle: nil),
            vc = storyboard.instantiateInitialViewController() as! FitnessRegistrationViewController
            vc.challenge = challenge
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }.catch { error in
            let alert = UIAlertController(title: "Unable to register Challenge", message: "localized description:\(error.localizedDescription)\ncancelled:\(error.isCancelled)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }.finally {
            MRProgressOverlayView.dismissOverlay(for: UIApplication.shared.keyWindow, animated: false)
        }
    }
    
    func loadFitnessChallengeDetail(challenge: LWChallengeCMS) {
        guard challenge.active == true else {return}
        MRProgressOverlayView.showOverlayAdded(to: self.view, title: "Loading Challenge...", mode: .indeterminateSmallDefault, animated: false)
        
        challenge.promiseForFitnessChallenge(id: challenge.id!).done { fitnessChallenge in
            LWFitnessChallenge.current = fitnessChallenge
            self.myProgress?.selectedWeek = LWFitnessChallenge.current.weeksIntoChallenge()
            self.myProgress?.recomputeUI()
            CitiNotificationCenter.default.post(Notification(name: Notification.Name(rawValue: LWNotificationNames.ActivityListUpdated)))
            
        }.catch { error in
            if let lwapiError = error as? LWAPIError{
                let alert = UIAlertController(title: "Unable to Load fitness Challenge", message: "additional data description:\(String(describing: lwapiError.additionalData))\n is cancelled:\(error.isCancelled)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            //TODO: PM uncomment
            let alert = UIAlertController(title: "Unable to Load fitness Challenge", message: "localized description:\(error.localizedDescription)\n is cancelled:\(error.isCancelled)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)

        }.finally {
            MRProgressOverlayView.dismissOverlay(for: self.view, animated: false)
        }
    }
    
    @IBAction func navigateToUser (_ sender: UIButton?){
        rightButtonsContinerViewController.childView = .settingsAndSync
        self.navigationController!.pushViewController(rightButtonsContinerViewController, animated: true)
        
    }
    private func setUIValues(){
        lblusername.text = "Welcome Back \(user.firstName ?? "")";
        let office = selectedOffice?.name ?? ""
        self.lbllocation.text = "Location: \(office)"
        self.checkChallnageRegistration()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let titleAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
                   NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue,
               ]
        let underlineAttributedString = NSAttributedString(string: "(Edit my profile)", attributes: titleAttributes)
        btnUserInfo.setAttributedTitle(underlineAttributedString, for: .normal)
        customPageControl.delegate = self
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Ensure that child view controller autolayout works
        segue.destination.view.translatesAutoresizingMaskIntoConstraints = false
        if segue.identifier == "LandingMyProgress" {
            self.myProgress = (segue.destination as! MyProgressViewController)
        }
    }
    public func loadUserFromAPI() {
        LWCMSAPI.landing.promise().done { landingResponse in
            if(landingResponse.slides != nil){
                DispatchQueue.main.async {
                    self.items = landingResponse.slides!
                    self.customPageControl.items = self.items
                    self.courosolCollection.reloadData()
                }
                
            }
        }.catch { error in
            print("error catch")
        }.finally {
            print("error finally")
        }
        LWWebAPI.getUser
            .promise()
            .done{ user in
                self.user = user.user!
                self.selectedOffice = LWUtil.getOffice(id: user.user?.officeLocationId)
                self.setUIValues()
                if(self.featuredChallenge.isRegistered){
                    self.loadFitnessChallengeDetail(challenge: self.featuredChallenge)
                }
        }.catch { (error) in
            // TODO: error handling
        }
    }
    @IBAction func gottoSepecifcPage(_ sender: UIButton){
        delegate!.navigationToPage(MainTabBarViewController.Views.challenges.rawValue)
        
        
    }
    

   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let user = LWMain.data.user, user.timezone == nil {
            CitiNotificationCenter.default.post(name: .setTimeZone, object: nil, userInfo: [:])
        }
    }
    

    @IBAction func mainButtonTapped(_ sender: UIButton) {
        //Patrick TODO: Track button click here.
        if let buttonType = self.lpageData.navigateTo.type{
            let object = { () -> MainTabBarViewController.Views in
                switch self.lpageData.navigateTo.type! {
                case .event:
                    Analytics.logEvent(LWAnalyticsHelper.kEventHomeHeroButton, parameters: [LWAnalyticsHelper.kButtonDestinationType:LWAnalyticsHelper.kButtonDestinationEvents])
                    return MainTabBarViewController.Views.events
                case .fitnessChallenge:
                    Analytics.logEvent(LWAnalyticsHelper.kEventHomeHeroButton, parameters: [LWAnalyticsHelper.kButtonDestinationType:LWAnalyticsHelper.kButtonDestinationFitnessChallenege])
                    return MainTabBarViewController.Views.challenges
                case .wellnessChallenge:
                    Analytics.logEvent(LWAnalyticsHelper.kEventHomeHeroButton, parameters: [LWAnalyticsHelper.kButtonDestinationType:LWAnalyticsHelper.kButtonDestinationWellnessChallenge])
                    return MainTabBarViewController.Views.challenges
                case .challenge:
                    Analytics.logEvent(LWAnalyticsHelper.kEventHomeHeroButton, parameters: [LWAnalyticsHelper.kButtonDestinationType:LWAnalyticsHelper.kButtonDestinationChallenges])
                    return MainTabBarViewController.Views.challenges
                case .article:
                    Analytics.logEvent(LWAnalyticsHelper.kEventHomeHeroButton, parameters: [LWAnalyticsHelper.kButtonDestinationType:LWAnalyticsHelper.kButtonDestinationArticleLanding])
                    return MainTabBarViewController.Views.articles
                case .faq:
                    Analytics.logEvent(LWAnalyticsHelper.kEventHomeHeroButton, parameters: [LWAnalyticsHelper.kButtonDestinationType:LWAnalyticsHelper.kButtonDestinationFAQs])
                    return MainTabBarViewController.Views.faqs
                case .about:
                    Analytics.logEvent(LWAnalyticsHelper.kEventHomeHeroButton, parameters: [LWAnalyticsHelper.kButtonDestinationType:LWAnalyticsHelper.kButtonDestinationAbout])
                    return MainTabBarViewController.Views.about
                }
                
            }()
            
            let userInfo = [
                "navigateToFitness": self.lpageData.navigateTo.type! == .fitnessChallenge,
                "navigateToWellness": self.lpageData.navigateTo.type! == .wellnessChallenge
            ]
            
            CitiNotificationCenter.default
                .post(
                    Notification(
                    name: NSNotification.Name(LWNotificationNames.NavigateToTab),
                    object: object,
                    userInfo: userInfo
                    )
            )
        }
        else {
            let alert = UIAlertController(title: "", message: "unable to load featured content", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: false, completion: nil)
            return        }
            
    }
}

extension LandingViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "HorizontalCell", for: indexPath) as! HorizontalCell
        let slide = items[indexPath.row] as LWSlides
        print(slide)
        cell.lbltilte.text = slide.subtitle!.uppercased()
        let string = slide.title ?? ""
        cell.lblDetail.text = "\(string)>>"
        let stringIndex = String(indexPath.row)
        cell.hideVideo()
        if(slide.image == nil){
            if(slide.videoId != nil){
                let videoLink = "https://lwcdev.com/static/youtube_player.html?videoID=\(slide.videoId!)"
                cell.loadVideo(videoLink)
            }
            else{
                cell.viewTitleHeight.constant = collectionView.frame.height
            }
        }
        else{
            cell.viewTitleHeight.constant = collectionView.frame.height/2
            cell.imgview!.loadDisplayImageFromURL(urlstring: slide.image!.imageURL!, withIndex: stringIndex, delegate: self, avatarImage: slide.imageLinkObvject, size: CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height/2))
        }
        cell.viewDesc.backgroundColor = hexStringToUIColor(hex: slide.color!)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x/self.courosolCollection.frame.width
        customPageControl.setSelectedIndex(Int(page) + 1)
    }
    
}
extension LandingViewController : AsyncImageViewDelegate{
    func downloadedImage(image: UIImage, atIndex: Int) {
        let slide = self.items[atIndex]
        slide.imageLinkObvject = image
        self.items[atIndex] = slide
    }

}


extension LandingViewController : CustomPageControlDelegate{
    func selectedpage(_ index: Int) {
        self.courosolCollection.selectItem(at: IndexPath(row: index, section: 0), animated: true, scrollPosition: .right)
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
extension UIColor {
    static func random() -> UIColor {
        return UIColor(
           red:   .random(),
           green: .random(),
           blue:  .random(),
            alpha: 0.2
        )
    }
}
func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }

    if ((cString.count) != 6) {
        return UIColor.gray
    }

    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
