//
//  ViewController.swift
//  Courosol
//
//  Created by Hitendra on 30/10/21.
//

import UIKit
import WebKit
struct slides {
    var title = ""
    var description = ""
    var imageLink = ""
    var videoLink = ""
    var imageLinkObvject : UIImage?
}

class HorizontalCell : UICollectionViewCell, WKUIDelegate{
    
    @IBOutlet weak var viewTitleHeight : NSLayoutConstraint!
    @IBOutlet weak var tiltetop : NSLayoutConstraint!
    @IBOutlet weak var viewDesc : UIView!
    @IBOutlet weak var lbltilte : UILabel!
    @IBOutlet weak var lblDetail : UILabel!
    @IBOutlet weak var imgview : AsyncImageView?
    
    lazy var webView: WKWebView = {
        let webConfiguration = WKWebViewConfiguration()
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.bringSubviewToFront(webView)
    }

    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        webView = WKWebView(frame: frame)
        self.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        webView.heightAnchor.constraint(equalToConstant: self.frame.width/2-6).isActive = true
       
    }
    
    func hideVideo(){
        webView.isHidden = true
    }
    func loadVideo(_ urlString : String){
        let width = "\(webView.frame.width)"
        let height = "\(webView.frame.height)"
        let url = "\(urlString)?enablejsapi=\"1"
        print("width,height",width,height)
        var embededHTML = "<html><body><iframe src=\"URL\" frameborder=\"0\" playsinline=\"1\"></iframe></body></html>"
        embededHTML = embededHTML.replacingOccurrences(of: "WIDTH", with: width)
        embededHTML = embededHTML.replacingOccurrences(of: "HEIGHT", with: height)
        embededHTML = embededHTML.replacingOccurrences(of: "URL", with: url)
//        webView.load(URLRequest.init(url: URL(string: urlString)!))
        print("embededHTML",embededHTML)
        webView.loadHTMLString(embededHTML, baseURL: Bundle.main.bundleURL)

        webView.isHidden = false
    }
    
}

class ViewController: UIViewController {

    @IBOutlet weak var courosolCollection : UICollectionView!
    @IBOutlet weak var customPageControl : CustomPageControl!
    var items : [slides] = [
        slides(title: "Slide 1", description: "First slide with Image", imageLink: "https://images.pexels.com/photos/417074/pexels-photo-417074.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500", videoLink: ""),
        slides(title: "Slide 2", description: "First slide with Image", imageLink: "https://images.pexels.com/photos/210186/pexels-photo-210186.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500", videoLink: ""),
        slides(title: "Slide 3", description: "First slide with Image", imageLink: "", videoLink: "https://www.youtube.com/embed/n-qRbUrG5ro?playsinline=1"),
        slides(title: "Slide 4", description: "First slide with Image", imageLink: "https://images.pexels.com/photos/6992/forest-trees-northwestisbest-exploress.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500", videoLink: ""),
        slides(title: "Slide 5", description: "First slide with Image", imageLink: "", videoLink: "https://www.youtube.com/embed/DZ-w7UMO194?playsinline=1")
    
    ];
    override func viewDidLoad() {
        super.viewDidLoad()
        customPageControl.items = self.items
        customPageControl.delegate = self
    }
}
extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "HorizontalCell", for: indexPath) as! HorizontalCell
        let slide = items[indexPath.row] as slides
        print(slide)
        cell.lbltilte.text = slide.title
        cell.lblDetail.text = slide.description
        let stringIndex = String(indexPath.row)
        cell.hideVideo()
        if(slide.imageLink.count == 0){
            //cell.viewTitleHeight.constant = collectionView.frame.height
            cell.loadVideo(slide.videoLink)
        }
        else{
            cell.viewTitleHeight.constant = collectionView.frame.height/2
            cell.imgview!.loadDisplayImageFromURL(urlstring: slide.imageLink, withIndex: stringIndex, delegate : self, avatarImage: slide.imageLinkObvject ,size: CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height/2))
        }
        cell.viewDesc.backgroundColor = .random()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x/self.courosolCollection.frame.width
        customPageControl.setSelectedIndex(Int(page) + 1)
    }
    
}

extension ViewController : AsyncImageViewDelegate{
    func downloadedImage(image: UIImage, atIndex: Int) {
        var slide = self.items[atIndex]
        slide.imageLinkObvject = image
        self.items[atIndex] = slide
    }

}

extension ViewController : CustomPageControlDelegate{
    func selectedpage(_ index: Int) {
        self.courosolCollection.selectItem(at: IndexPath(row: index, section: 0), animated: true, scrollPosition: .right)
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
extension UIColor {
    static func random() -> UIColor {
        return UIColor(
           red:   .random(),
           green: .random(),
           blue:  .random(),
            alpha: 0.2
        )
    }
}
