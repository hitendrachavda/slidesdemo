//
//  AsyncImageView.swift
//

import Foundation
import UIKit

@objc protocol AsyncImageViewDelegate{
    func downloadedImage(image : UIImage, atIndex : Int)
}

class AsyncImageView: UIView {

    var session : URLSession?
    var downloadTask : URLSessionDownloadTask?
    var internalImageView : UIImageView?
    var activityIndicator : UIActivityIndicatorView?
    
    @objc var delegate: AsyncImageViewDelegate? = nil
    
     override init(frame: CGRect) {
         super.init(frame: frame)
         self.setupAsynchView()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    override func awakeFromNib() {
       super.awakeFromNib()
       self.setupAsynchView()
    }
    
    func setupAsynchView(){
        if session == nil {
            let sessionConfig  = URLSessionConfiguration.default
            session = URLSession.init(configuration: sessionConfig)
        }
        self.addNewImageView()
    }
    
    func addNewImageView(){
        self.internalImageView?.removeFromSuperview()
        let internalImageView = UIImageView.init(frame: self.bounds)
        internalImageView.contentMode = .scaleAspectFill
        internalImageView.clipsToBounds = true
        
        internalImageView.backgroundColor = .clear
        internalImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(internalImageView)
        
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.tintColor = .black
        activityIndicator.startAnimating()
        activityIndicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        activityIndicator.center = self.center
        self.addSubview(activityIndicator)
        
        self.activityIndicator = activityIndicator
        self.activityIndicator?.isHidden = true
        self.internalImageView = internalImageView
    
    }
    
    func setImage(image : UIImage?){
        DispatchQueue.main.async {
            self.internalImageView!.image = image;
        }
    }
    func setactivityIndicator(isHidden : Bool){
        DispatchQueue.main.async {
            self.activityIndicator?.isHidden = isHidden
            if(!isHidden){
                self.activityIndicator?.startAnimating()
            }
        }
    }
    func fetchImage(fromDocumentsDirectory imageName: String?) -> String? {
        let imagePath = URL(fileURLWithPath: documentsDirectoryPath()!).appendingPathComponent(imageName ?? "").path
        return imagePath
    }
    func loadDisplayImageFromURL(urlstring : String , withIndex: String, delegate : AsyncImageViewDelegate, avatarImage : UIImage?, size :CGSize){
        self.delegate = delegate
        let url = URL(string: urlstring)
        if(avatarImage != nil){
            self.setImage(image:avatarImage)
            self.setactivityIndicator(isHidden: true)
        }
        else{
            self.setImage(image: nil)
            self.setactivityIndicator(isHidden: false)
            downloadTask?.cancel()
            downloadTask = session!.downloadTask(with: url!, completionHandler: { [self] location, response, error in
                var data: Data? = nil
                if let location = location {
                    data = try! Data(contentsOf: location)
                }
                if let data = data {
                    let image = UIImage(data: data)
                    if let image = image {
                        let img = self.image(with: image, convertTo: size)
                        if let img = img {
                            DispatchQueue.main.async {
                                self.delegate?.downloadedImage(image: img, atIndex: Int(withIndex)!)
                                self.setImage(image: img)
                                self.setactivityIndicator(isHidden: true)
                            }
                        }
                    }
                }
            })
            downloadTask!.resume()
        }
    }
    
    func image(with image: UIImage?, convertTo size: CGSize) -> UIImage? {
        if (image?.size.width ?? 0.0) > size.width {
            UIGraphicsBeginImageContext(size)
            image?.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let destImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return destImage
        }
        return image
    }
    
    func saveImageToDocumentDirectory(with capturedImage: UIImage?, imageName: String?) {
        let dataPath = self.documentsDirectoryPath()

        //Create a folder inside Document Directory
        if !FileManager.default.fileExists(atPath: dataPath!) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath!, withIntermediateDirectories: false, attributes: nil)
            } catch {
            } //Create folder
        }

        let imagePath = "\(dataPath ?? "")/\(imageName ?? "")"
        if !FileManager.default.fileExists(atPath: imagePath) {
            var imageDate: Data? = nil
            if let image = capturedImage?.pngData() {
                imageDate = Data(image)
            }
            if let imageDate = imageDate {
                NSData(data: imageDate).write(toFile: imagePath, atomically: true)
            }
        }
    }
    
    func documentsDirectoryPath() -> String? {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).map(\.path)
        let documentsDirectory = paths[0] // Get documents folder
        let dataPath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("images").path
        return dataPath
    }

    
}
